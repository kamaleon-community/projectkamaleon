package org.projectkamaleon;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjectkamaleonApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectkamaleonApplication.class, args);
	}
}
